<!DOCTYPE html>
<html style="filter: blur(0px);">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<title>Home - Professional Networker</title>
<meta name="description"
	content="This site is designed for CST-256 as a professional networking webpage demo built on Laravel">
<link rel="icon" type="image/jpeg" sizes="1900x1250"
	href="bootstrap/img/header-bg.jpg">
<link rel="stylesheet" href="bootstrap/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Kaushan+Script">
<link rel="stylesheet" href="bootstrap/fonts/font-awesome.min.css">
<link rel="stylesheet" href="bootstrap/fonts/ionicons.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
<link rel="stylesheet" href="bootstrap/css/styles.min.css">
</head>

<body id="page-top" style="filter: blur(0px);">
	<nav class="navbar navbar-dark navbar-expand-lg fixed-top bg-dark"
		id="mainNav"
		style="padding-bottom: 20px; padding-top: 20px; box-shadow: 0px 0px 11px rgba(254, 209, 54, 0.12);">
		<div class="container">
			<a class="navbar-brand swing animated" href="{{route('/')}}">Professional
				Networker</a>
			<button data-toggle="collapse" data-target="#navbarResponsive"
				class="navbar-toggler navbar-toggler-right" type="button"
				aria-controls="navbarResponsive" aria-expanded="false"
				aria-label="Toggle navigation"
				style="margin-bottom: 12px; margin-top: 12px;">
				<i class="fa fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto text-uppercase">
					<li class="nav-item"><a
						class="nav-link border rounded-0 border-primary js-scroll-trigger"
						href="login" style="text-align: center; margin-right: 24px;">Login</a></li>
					<li class="nav-item"><a
						class="nav-link border rounded-0 border-primary js-scroll-trigger"
						href="register"
						style="filter: blur(0px); text-align: center; margin-right: 24px;">Register</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<header class="masthead"
		style="background-image: url('bootstrap/img/header-bg.jpg');">
		<div class="container">
			<div class="intro-text">
				<div class="intro-lead-in">
					<span>It shouldn't be hard!</span>
				</div>
				<div class="intro-heading text-uppercase">
					<span data-bss-hover-animate="shake">After all, we're better
						together</span>
				</div>
				<a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
					role="button" data-bss-hover-animate="rubberBand" href="register">Jump
					in!</a>
			</div>
		</div>
	</header>
	<section class="py-5"
		style="text-align: center; border-color: rgb(33, 37, 41); border-top-color: rgb(33,; border-right-color: 37,; border-bottom-color: 41); border-left-color: 37,; background: var(- -dark);">
		<small style="margin: auto; padding: auto; color: #4a4f55;">Copyright
			2021&nbsp;(©) Joshua Beck, Mark Pratt, Shawn Fradet</small>
	</section>
	<script src="bootstrap/js/jquery.min.js"></script>
	<script src="bootstrap/bootstrap/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
	<script src="bootstrap/js/script.min.js"></script>
</body>

</html>