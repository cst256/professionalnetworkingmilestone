<?php
/*
 * Group 1 Milestone 1
 * UserModel.php Version 1
 * CST-256
 * 4/16/2021
 * This Model represents a User for the site.
 */
namespace App\Models;

class UserModel
{

    private $userID;

    private $firstName;

    private $lastName;

    private $userName;

    private $email;

    private $password;

    /**
     * UserModel constructor.
     *
     * @param
     *            $firstName
     * @param
     *            $lastName
     * @param
     *            $userName
     * @param
     *            $emailAddress
     */
    public function __construct($firstName, $lastName, $userName, $emailAddress, $password)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->userName = $userName;
        $this->email = $emailAddress;
        $this->password = $password;
    }

    // Getters and setters
    /**
     *
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     *
     * @param mixed $userID
     */
    public function setUserID($userID): void
    {
        $this->userID = $userID;
    }

    /**
     *
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     *
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     *
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     *
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     *
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     *
     * @param mixed $userName
     */
    public function setUserName($userName): void
    {
        $this->userName = $userName;
    }

    /**
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     *
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     *
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
}
